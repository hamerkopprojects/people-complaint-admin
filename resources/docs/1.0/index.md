- ## Get Started
    - [Overview](/{{route}}/{{version}}/overview)

- ## Login
    - [Login](/{{route}}/{{version}}/auth/login)
    - [Logout](/{{route}}/{{version}}/auth/logout)

- ## Complaint
    - [list](/{{route}}/{{version}}/complaint/complaint-list)
    - [Details](/{{route}}/{{version}}/complaint/details)
    - [Register](/{{route}}/{{version}}/complaint/register)
    - [Ward Listing](/{{route}}/{{version}}/complaint/ward-list)
