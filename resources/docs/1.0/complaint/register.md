# Register Complaint

---

Complaint Registration

### Details

| Method | Uri                              | Authorization |
| :----- | :------------------------------- | :------------ |
| POST   | `api/complaint` | No         |

### Request Params

Form Data

| param | value                          |
| :---- | :----------------------------- |
| first_name | Reshma                    |
| last_name  | Ravi |
|phone|4561237895|
|house_no|391A|
|address|xyz home|
|title|Complaint|
|description| DEscriptionn|
|file|image|
|ward_id|ward_id|

### Response

```json
{
    "success": true,
    "msg": "Updated Successfully"
}
```
