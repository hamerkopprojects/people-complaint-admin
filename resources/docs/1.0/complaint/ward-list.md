# List Ward

---
Ward list

### Details

| Method | Uri   | Authorization |
| : |   :-   |  :  |
| GET | `api/list-ward` | No |

### Request Params
```json
{
     "language":"mal" // for malayalam 'en'=> for english
}
```
### Response

```json
{
    "success": true,
    "data": [
        {
            "id": 1,
            "status": "active",
            "lang": [
                {
                    "id": 2,
                    "ward_id": 1,
                    "name": "ചന്ദവില",
                    "language": "mal"
                }
            ]
        },
        {
            "id": 2,
            "status": "active",
            "lang": [
                {
                    "id": 4,
                    "ward_id": 2,
                    "name": "കട്ടായിക്കോണം",
                    "language": "mal"
                }
            ]
        }
    ]
}
```
