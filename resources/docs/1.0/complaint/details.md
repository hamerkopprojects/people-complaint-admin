# Complaint details
---
### Details
| Method | Uri   | Authorization |
| : |   :-   |  :  |
| GET | `api/complaintDetails/{complaint-id}` | yes |

### Response
```json
{
    "success": true,
    "data": {
        "id": 1,
        "user_id": 1,
        "title": "my complaint",
        "description": "heavy",
        "file": "http://localhost/people-complaint-admin/public/storage/photos/2Sjbt0vO49eHMwn08eUPmijNFrr7ULlAxDbvYmjP.jpeg",
        "status": "pending",
        "created_at": "2020-10-28T13:56:06.000000Z",
        "updated_at": "2020-10-28T13:56:06.000000Z",
        "deleted_at": null,
        "users": {
            "id": 1,
            "user_name": null,
            "first_name": "reshma",
            "last_name": "ravi",
            "email": null,
            "email_verified_at": null,
            "phone": "9995256",
            "house_no": "12",
            "address": null,
            "role": "user",
            "created_at": "2020-10-28T13:56:06.000000Z",
            "updated_at": "2020-10-28T13:56:06.000000Z",
            "deleted_at": null
        }
    }
}
```
