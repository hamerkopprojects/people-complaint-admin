# List Complaints

---
Complaint list

### Details

| Method | Uri   | Authorization |
| : |   :-   |  :  |
| GET | `api/list-complaint` | yes |

### Request Params
```json
{
     "date":"28-10-2020" ,// for filter 
     "ward":"1"// ward id for filter
}
```
### Response

```json
{
    "success": true,
    "data": {
        "current_page": 1,
        "data": [
            {
                "title": "my complaint",
                "description": "heavy",
                "file": "http://localhost/people-complaint-admin/public/storage/photos/2Sjbt0vO49eHMwn08eUPmijNFrr7ULlAxDbvYmjP.jpeg",
                "id": 1
            }
        ],
        "first_page_url": "http://localhost/people-complaint-admin/public/api/list-complaint?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://localhost/people-complaint-admin/public/api/list-complaint?page=1",
        "next_page_url": null,
        "path": "http://localhost/people-complaint-admin/public/api/list-complaint",
        "per_page": 50,
        "prev_page_url": null,
        "to": 1,
        "total": 1
    }
}
```
