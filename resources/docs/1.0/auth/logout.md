# Logout

---
Logout

### Details

| Method | Uri   | Authorization |
| : |   :-   |  :  |
| POST | `api/logout` | Yes |

### Request Params

No params

### Response

```json
{
    "success": true,
    "msg": "Logout Successfully"
}
```
