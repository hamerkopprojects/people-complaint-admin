# Login

---

 Login 

### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| POST   | `api/login` | No            |

### Request Params

```json
{
     "username":"reshmaravi",
    "password":"1234"
}
```

### Response

```json
{
     "data": {
        "user": {
            "id": 3,
            "user_name": "reshmaravi",
            "first_name": null,
            "last_name": null,
            "email": null,
            "email_verified_at": null,
            "phone": null,
            "house_no": null,
            "address": null,
            "role": "admin",
            "created_at": null,
            "updated_at": null,
            "deleted_at": null
        },
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiYTUzMDY5YzNjMDgxOWQxMTM1MGYzOTAyZTE5Y2FlMzk1MjkzMzk0ZmM0YmIwZDM4M2VkMDQ0ODgxYzBiYjQxZTFkMmMxY2Q2OWFiYmI4YTYiLCJpYXQiOjE2MDM5NTM5NzgsIm5iZiI6MTYwMzk1Mzk3OCwiZXhwIjoxNjM1NDg5OTc3LCJzdWIiOiIzIiwic2NvcGVzIjpbImFkbWluIl19.dx04rvO0R303O52oDAXkaZQdzSl_j0XA2JdPOKr0l3eJR0Y704fQS-PjyRBfDpQm-uhgQ4MiNdM9RkTZY9rR0vGIDfcSi9VmrbAQMAT2FO_E_t2GSiTVRHG5b1TGh86z6cmw0vdBVzwIxwEnMCNfXLyVzTrxediut0QsG_ABuPtoKvhD8kd8cFmigudVErjV5N4V0K_6Utdi9dpBGbqMzRipAFfyefMrS-z-HKAs9iSUi4mZMwSKC_L-rysJFQaTDzqJP3K59J0WTsJJzYwSiVZpWUcomIW9Vo3sHjrsD14mdttOtZ1nVJFlc3lYvT6_OeACE7VqOXtsgbnJnf8B4VyF1l5tEsxQ4PJ_2F9qig_pWdDWP4b-v-u5YOVZOU0SwKcbStX4MNKj2p-HTPjuAyrA9kp6lyzBWspfERqdROfgDv4Enhirg4mK8ljRC8JmkQGyV0NOMEPKTp9IuuVvr_Li-RWUEoRjyCdz38cX39ppw6sTEe61LhZ6sR6TX_dMJHcd1fUyh9488OD5YpVpu6z01LohQJtMmK6fsTYLbD791cTJS-hs1Io-cRE3_FDtybaC_MZuQ3v10Mc6ayKJcKmywtE2wKZy5UY6VSE8Aeb2jHnlzNsUXSZ9Vy2K_LEYYzU1B12M2P6LaD8VwmCJle07NGtzStRqpaDH01dotic"
    }
}
```
