<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WardLang extends Model
{
    use SoftDeletes ;

    protected $table = "ward_i18n";

    protected $guarded = [];
}
