<?php

namespace App\Models;

use App\User;
use App\Models\Ward;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Complaints extends Model
{
    use SoftDeletes ;

    protected $table = 'complaints';
    
    protected $guarded = [] ;

    public function getFileAttribute($path)
    {
        // dd($path);
        return is_null($path) 
            ? null
            : asset(url('/uploads/'.$path));
            // asset(Storage::url($path));
    }

    public function users()
    {
        return $this->BelongsTo(User::class,'user_id');
    }
    public function ward()
    {
        return $this->BelongsTo(Ward::class,'ward_id');
    }
}
