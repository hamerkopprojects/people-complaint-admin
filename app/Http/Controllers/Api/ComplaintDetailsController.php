<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\Complaints;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessWithData;
use App\Http\Requests\Api\ConplaintListRequest;
use App\Traits\FormatLanguage;

class ComplaintDetailsController extends Controller
{
     use FormatLanguage;

    public function listComplaints(ConplaintListRequest $req)
    {
        $date=$req->date;
        $ward = $req->ward ;
        $complaintList=Complaints::with(['users' => function ($query) {
                    $query->select(DB::raw('CONCAT(first_name, " ",last_name) AS name'),'id');
            }
            ])
            
            ->where('deleted_at',null)
            ->when($req->has('date'), function ($query) use ($date) {
              
                $query->whereDate('updated_at',Carbon::parse($date)->format('Y-m-d'));
            })
            ->when($req->has('ward'), function ($query) use ($ward) {
              
                $query->where('ward_id',$ward);
            })
            ->select('title','description','file','id','created_at','user_id')
            ->orderBy('created_at', 'DESC')
            ->paginate(50);

        return new SuccessWithData($complaintList);
    }

    public function getDetails($id)
    {
           $details= Complaints::with('users')
           ->with(['ward' => function ($query) {
                $query->with('lang:name,id,ward_id,language');
           
            }])
            ->where('id',$id)
            ->first();
            $this->keyBy($details->ward, 'lang', 'language');

            return new SuccessWithData($details);
    }
}
