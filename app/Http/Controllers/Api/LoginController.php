<?php

namespace App\Http\Controllers\Api;

use App\User;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Responses\ErrorResponse;
use App\Http\Requests\Api\LoginRequest;
use App\Http\Responses\SuccessWithData;
use App\Http\Responses\SuccessResponseMessage;

class LoginController extends Controller
{
    protected $user;
    
    public function __construct(User $user)
    {
        $this->user =$user;
    }
    public function login(LoginRequest $req)
    {
        $admin = $this->user
            ->where('user_name', $req->username)
            ->where('role','admin')
            ->first();
            $noUserFound = is_null($admin);

            if ($noUserFound) {
                throw new Exception('User does not exists');
            }
            
            if(Hash::check($req->password,$admin->password)) {
                
                $token = $admin->createToken('Api Token', ['admin'])->accessToken;
                return new SuccessWithData([
                    'user' => $admin,
                    'access_token' => $token,
                ]);
            }else{
                return new ErrorResponse("Enter correct username / password");
            }
    }

    

    public function logout()
    {
        $admin = auth()->user();
        $admin->token()->revoke();

        return new SuccessResponseMessage('Logout Successfully');
    }
}
