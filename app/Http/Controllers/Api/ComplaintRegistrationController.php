<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\PersonalDataRequest;
use App\Http\Responses\SuccessResponseMessage;
use Dotenv\Result\Success;

class ComplaintRegistrationController extends Controller
{
    public function addComplaint(PersonalDataRequest $req)
    {
        $details = DB::transaction(function () use ($req) {
                $complaints=User::create([
                    'first_name'=>$req->first_name,
                    'last_name' =>$req->last_name,
                    'phone'=>$req->phone,
                    'house_no'=>$req->house_no,
                    'address'=>$req->address,
                    'role'=>'user'
                ]);
                $complaints->commplaints()->createMany([
                    [
                        'title'=>$req->title,
                        'description'=>$req->description,
                        'file'=>$req->file('file')->store('public/complaint-photos',['disk' => 'public_uploads']),
                        'status'=>'pending',
                        'ward_id'=>$req->ward_id
                    ]
                    
                ]);
        });

        return new SuccessResponseMessage('Updated Successfully');
        
    }
}
