<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessWithData;
use App\Models\Ward;
use Illuminate\Http\Request;

class WardDetailsController extends Controller
{
    public function list(Request $req)
    {
        $lang= $req->language ?? 'en';
        $ward= Ward::with(['lang' => function ($query) use ($lang) {
            $query->where('language',$lang)
            ->select('id','ward_id','name','language');
        }])
        ->where('status','active')
        ->select('id','status')
        ->get();

        return new SuccessWithData($ward);
    }
}
