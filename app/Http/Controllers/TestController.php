<?php

namespace App\Http\Controllers;

use App\Models\Complaints;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    public function get()
    {
        echo(asset(''));
        $complaintList=Complaints::with(['users' => function ($query) {
            $query->select(DB::raw('CONCAT(first_name, " ",last_name) AS name'),'id');
    }
    ])
    
    ->where('deleted_at',null)
    
    ->select('title','description','file','id','created_at','user_id')
    ->orderBy('created_at', 'DESC')
    ->first();
    
    return view('hii',compact('complaintList'));
}
}
