<?php

use App\Http\Controllers\Api\ComplaintDetailsController;
use App\Http\Controllers\Api\ComplaintRegistrationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\WardDetailsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [LoginController::class, 'login']);

Route::post('complaint', [ComplaintRegistrationController::class, 'addComplaint']);
Route::get('list-ward', [WardDetailsController::class, 'list']);


Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', [LoginController::class, 'logout']);
    Route::get('list-complaint', [ComplaintDetailsController::class, 'listComplaints']);
    Route::get('complaintDetails/{id}', [ComplaintDetailsController::class, 'getDetails']);
});